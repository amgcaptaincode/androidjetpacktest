package com.code.ald.androidjetpacktest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;


public class MyNoteRecyclerViewAdapter extends RecyclerView.Adapter<MyNoteRecyclerViewAdapter.ViewHolder> {

    private final List<Note> noteList;
    private final NoteInteractionListener mListener;

    public MyNoteRecyclerViewAdapter(List<Note> notes, NoteInteractionListener listener) {
        noteList = notes;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_note, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = noteList.get(position);
        holder.tvTitle.setText(holder.mItem.getTitle());
        holder.tvContent.setText(holder.mItem.getContent());

        if (holder.mItem.getFavorite())
            holder.imgvView.setImageResource(R.drawable.ic_star_black_24dp);

        holder.imgvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.favoriteNoteClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvTitle;
        public final TextView tvContent;
        public final ImageView imgvView;
        public Note mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvTitle     = view.findViewById(R.id.tvTitle);
            tvContent   =  view.findViewById(R.id.tvContent);
            imgvView     = (ImageView) view.findViewById(R.id.imgvFavorite);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvTitle.getText() + "'";
        }
    }
}
