package com.code.ald.androidjetpacktest;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class NoteFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 2;
    private NoteInteractionListener mListener;

    private List<Note> noteList;

    private MyNoteRecyclerViewAdapter mAdapter;

    public NoteFragment() {
    }

    public static NoteFragment newInstance(int columnCount) {
        NoteFragment fragment = new NoteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (view.getId() == R.id.listPortrait) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {

                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
                int numColums = (int) (dpWidth / 180);
                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(numColums, StaggeredGridLayoutManager.VERTICAL));
            }

            noteList = new ArrayList<>();
            noteList.add(new Note("Lista de compras","pan tostado", true, android.R.color.holo_blue_bright));
            noteList.add(new Note("Recordatorio","Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", false, android.R.color.holo_purple));
            noteList.add(new Note("Cumpleaños","Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", true, android.R.color.holo_purple));
            noteList.add(new Note("Lista de compras","pan tostado", true, android.R.color.holo_blue_bright));
            noteList.add(new Note("Recordatorio","Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", false, android.R.color.holo_purple));
            noteList.add(new Note("Cumpleaños","Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", true, android.R.color.holo_purple));
            noteList.add(new Note("Lista de compras","pan tostado", true, android.R.color.holo_blue_bright));
            noteList.add(new Note("Recordatorio","Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", false, android.R.color.holo_purple));
            noteList.add(new Note("Cumpleaños","Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", true, android.R.color.holo_purple));
            noteList.add(new Note("Recordatorio","Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", false, android.R.color.holo_purple));
            noteList.add(new Note("Cumpleaños","Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", true, android.R.color.holo_purple));
            noteList.add(new Note("Lista de compras","pan tostado", true, android.R.color.holo_blue_bright));
            noteList.add(new Note("Recordatorio","Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", false, android.R.color.holo_purple));
            noteList.add(new Note("Cumpleaños","Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", true, android.R.color.holo_purple));

            mAdapter = new MyNoteRecyclerViewAdapter(noteList, mListener);
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NoteInteractionListener) {
            mListener = (NoteInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NoteInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
